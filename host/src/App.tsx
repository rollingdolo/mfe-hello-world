import React from "react";
import ReactDOM from "react-dom";
import { Greet } from "serve/Greet";
import LogMe from "./LogMe"
import "./index.css";
import List from "serve/List"


const App = () => (
  <div className="container">
    I'm the container
    <LogMe />
    <Greet />
    <List />
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
