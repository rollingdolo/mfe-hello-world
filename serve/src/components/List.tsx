import React from 'react'
import {useStyles} from './Styles.'

const cities = [{name: 'New York',}, {name: 'London',}]

export default function List() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            {cities.map(c => {
                return <div>{c.name}</div>
            })}
        </div>
    )
}
