import {Theme} from "@mui/material";
import {makeStyles} from "@mui/styles";

export const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        backgroundColor: 'red',
        width: theme.spacing(25),
        border: "1px solid #e0e0e0",
        
    }}));