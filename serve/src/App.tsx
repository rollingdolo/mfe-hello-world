import React from "react";
import ReactDOM from "react-dom";
import {Greet} from "./components/Greet"
import "./index.css";
import { red, blue } from '@mui/material/colors';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import {Logger} from "./components/Logger.log";
import List from "./components/List";

Logger();
const theme = createTheme({
  palette: {
    primary: {
      main: red[500],
      
    },
    secondary: {
      main: blue[500],
    }

  },
});


const App:React.FC = () => {
  
  return(
    <ThemeProvider theme={theme}>
      <div>
    I'm the remote
    <Greet />
    <List />
  </div>
  </ThemeProvider>
  )
  };

ReactDOM.render(<App />, document.getElementById("app"));
